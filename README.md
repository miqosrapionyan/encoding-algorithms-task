# Getting Started

This project is a task for interview.
Project done using VueJS, and PHP, (dont worry nothing need to install: used cdn)

## Installation

- clone repo,
- cd to cloned folder
- open terminal and write `php -S localhost:8000`

## Usage
Open browser with this link `http://localhost:8000/index.html`
Enjoy the application :pizza:

## Application structure

There is same code written in functional programing and OOP.
To use functional version change the browser link to `http://localhost:8000/functional_version_backend/index.html`

Thanks!

