<?php
require_once './Interfaces/EncodingInterface.php';
require_once './Interfaces/SanitizeInputInterface.php';
require_once './Traits/TestInputTrait.php';
require_once './Traits/SubstitutionEncodingValidateTrait.php';

$_POST = json_decode(file_get_contents("php://input"),true);

class SubstitutionEncodingAlgorithm implements EncodingInterface, SanitizeInputInterface {

    use TestInputTrait, SubstitutionEncodingValidateTrait;

    /*
     * The post data
     */
    private $post_data;

    public function __construct($post){
        // Sanitize input values
        foreach($post as $key => $data){
            $this->post_data[$key] = $this->test_input($data);
        }
    }

    /*
     * Encodes the string
     */
    public function encode(): string {
        // If validation fails, its stops here
        $this->validate();

        //return str_replace($character, $replace_with, $value);
        $str_len = strlen($this->post_data['value']);
        $new_str = '';
        for ($i = 0; $i < $str_len; $i++){
            if($this->post_data['value'][$i] == $this->post_data['character']){
                $new_str .= $this->post_data['replace_with'];
            }else{
                $new_str .= $this->post_data['value'][$i];
            }
        }

        return $new_str;
    }
}

if(isset($_POST['value'])){
    $algorithm = new SubstitutionEncodingAlgorithm($_POST);

    $result = $algorithm->encode();
    http_response_code(200);
    echo json_encode(['result' => $result]);
}
