<?php

require_once './Interfaces/EncodingInterface.php';
require_once './Interfaces/SanitizeInputInterface.php';
require_once './Traits/TestInputTrait.php';
require_once './Traits/CompositeEncodingValidateTrait.php';

$_POST = json_decode(file_get_contents("php://input"),true);

class CompositeEncodingAlgorithm implements EncodingInterface, SanitizeInputInterface {

    use TestInputTrait, CompositeEncodingValidateTrait;

    /*
     * The post data
     */
    private $post_data;

    public function __construct($post){
        // Sanitize input values
        $this->post_data['value'] = $this->test_input($post['value']);
        foreach($post['algorithms'] as $key => $data){
            $this->post_data['algorithms'][$key] = $this->test_input($data);
        }
    }

    /*
     * Encodes the string
     */
    public function encode(): string {
        // If validation fails, its stops here
        $this->validate();

        $algorithms = [
            'md5_encode',
            'sha1_encode',
            'sha256_encode',
        ];

        foreach($this->post_data['algorithms'] as $index){
            $func_name = $algorithms[$index];
            $this->post_data['value'] = $this->$func_name();
        }

        return $this->post_data['value'];
    }

    private function md5_encode(): string {
        return md5($this->post_data['value']);
    }

    private function sha1_encode(): string {
        return hash('sha1', $this->post_data['value']);
    }

    private function sha256_encode(): string{
        return hash('sha256', $this->post_data['value']);
    }

}
if(isset($_POST['algorithms'])){
    $algorithm = new CompositeEncodingAlgorithm($_POST);

    $result = $algorithm->encode();
    http_response_code(200);
    echo json_encode(['result' => $result]);
}
