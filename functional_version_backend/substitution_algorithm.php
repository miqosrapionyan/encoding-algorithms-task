<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once 'test_input.php';
$_POST = json_decode(file_get_contents("php://input"),true);

if(isset($_POST['value'])){
    $value = test_input($_POST['value']);
    $character = test_input($_POST['character']);
    $replace_with= test_input($_POST['replace_with']);
    $message = '';

    if(!$value || $character === '' || $replace_with === '') {
        http_response_code(422);
        $message = 'Please fill all required fields. *';
        echo json_encode(['message' => $message]);
        die;
    }
    if(strlen($value) > 1000){
        http_response_code(422);
        $message = 'The encoding value length must be less or equal to 1000.';
        echo json_encode(['message' => $message]);
        die;
    }
    if(strlen($character) > 1 || strlen($replace_with) > 1){
        $message = 'The characters mut be only 1.';
        echo json_encode(['message' => $message]);
        die;
    }

    $result = encode($value, $character, $replace_with);

    http_response_code(200);
    echo json_encode(['result' => $result]);

    die;

}

function encode ($value, $character, $replace_with){
    //return str_replace($character, $replace_with, $value);
    $str_len = strlen($value);
    $new_str = '';
    for ($i = 0; $i < $str_len; $i++){
        if($value[$i] == $character){
            $new_str .= $replace_with;
        }else{
            $new_str .= $value[$i];
        }
    }

    return $new_str;
}



