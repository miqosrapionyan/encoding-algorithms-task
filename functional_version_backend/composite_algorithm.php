<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once 'test_input.php';

$_POST = json_decode(file_get_contents("php://input"),true);

if(isset($_POST['algorithms'])){
    $value = test_input($_POST['value']);
    $algorithm_indexes = $_POST['algorithms'];
    $message = '';
    if(!$value || empty($algorithm_indexes)) {
        http_response_code(422);
        $message = 'Please fill all required fields. *';
        echo json_encode(['message' => $message]);
        die;
    }
    if(strlen($value) > 1000){
        http_response_code(422);
        $message = 'The encoding value length must be less or equal to 1000.';
        echo json_encode(['message' => $message]);
        die;
    }
    encode($value, $algorithm_indexes);
    $result = $value;
    http_response_code(200);
    echo json_encode(['result' => $result]);

    die;

}

function md5_encode($value){
    return md5($value);
};

function sha1_encode($value){
    return hash('sha1', $value);
};

function sha256_encode($value){
    return hash('sha256', $value);
};

function encode (&$value, $algorithm_indexes){
    $algorithms = [
        'md5_encode',
        'sha1_encode',
        'sha256_encode',
    ];
    foreach($algorithm_indexes as $index){
        $value = $algorithms[$index]($value);
    }
}

