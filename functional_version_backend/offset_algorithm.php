<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once 'test_input.php';
$_POST = json_decode(file_get_contents("php://input"),true);

if(isset($_POST['offset'])){
    $value = test_input($_POST['value']);
    $offset = (int)test_input($_POST['offset']);
    $message = '';
    if(!$value || $offset === '') {
        http_response_code(422);
        $message = 'Please fill all required fields. *';
        echo json_encode(['message' => $message]);
        die;
    }
    if(strlen($value) > 1000){
        http_response_code(422);
        $message = 'The encoding value length must be less or equal to 1000.';
        echo json_encode(['message' => $message]);
        die;
    }
    if(!preg_match("/^[0-9]+/", $offset)){
        http_response_code(422);
        $message = 'The offset value must be number.';
        echo json_encode(['message' => $message]);
        die;
    }
    if($offset > 1000){
        http_response_code(422);
        $message = 'The offset value must be less or equal to 1000.';
        echo json_encode(['message' => $message]);
        die;
    }
    if($offset < 0){
        http_response_code(422);
        $message = 'The offset value cant be negative.';
        echo json_encode(['message' => $message]);
        die;
    }
    $result = encode($value, $offset + 1);

    http_response_code(200);
    echo json_encode(['result' => $result]);

    die;

}

function encode ($value, $offset){
    $len = strlen($value);
    if($offset > $len){
        $offset -= $len;
        return encode($value, $offset);
    }else{
        $arr = [];
        $str_len = strlen($value);
        for ($i = 0; $i < $str_len; $i++){
            $pos = $i + $offset;
            if($pos > $str_len){
                $pos -= $str_len;
            }
            $arr[$pos] = $value[$i];
        }
        ksort($arr);
        return join("", $arr);
    }
}
