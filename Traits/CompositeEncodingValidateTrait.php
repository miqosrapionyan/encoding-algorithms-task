<?php

trait CompositeEncodingValidateTrait {

    private function validate () {
        // The input values must present
        $this->check_not_empty();

        // The value lengh must be less or equal to 1000
        $this->check_value_length();
    }

    private function check_not_empty() {
        if(!$this->post_data['value'] || empty($this->post_data['algorithms'])) {
            http_response_code(422);
            $message = 'Please fill all required fields. *';
            echo json_encode(['message' => $message]);
            die;
        }
    }

    private function check_value_length() {
        if(strlen($this->post_data['value']) > 1000){
            http_response_code(422);
            $message = 'The encoding value length must be less or equal to 1000.';
            echo json_encode(['message' => $message]);
            exit;
        }
    }


}