<?php

trait OffsetEncodingValidateTrait {

    private function validate () {
        // The input values must present
        $this->check_not_empty();

        // The value lengh must be less or equal to 1000
        $this->check_value_length();

        // The offset value must be number
        $this->check_for_number();

        // Offset value cant be grater then 1000
        $this->check_offset_value_size();

        // Offset value cant be negative
        $this->check_offset_not_negative();
    }

    private function check_not_empty() {
        if(!$this->post_data['value'] || $this->post_data['offset'] === '') {
            http_response_code(422);
            $message = 'Please fill all required fields. *';
            echo json_encode(['message' => $message]);
            exit;
        }
    }

    private function check_value_length() {
        if(strlen($this->post_data['value']) > 1000){
            http_response_code(422);
            $message = 'The encoding value length must be less or equal to 1000.';
            echo json_encode(['message' => $message]);
            exit;
        }
    }

    private function check_for_number() {
        if(!preg_match("/^[0-9]+/", $this->post_data['offset'])){
            http_response_code(422);
            $message = 'The offset value must be number.';
            echo json_encode(['message' => $message]);
            exit;
        }
    }

    private function check_offset_value_size() {
        if($this->post_data['offset'] > 1000){
            http_response_code(422);
            $message = 'The offset value must be less or equal to 1000.';
            echo json_encode(['message' => $message]);
            exit;
        }
    }

    private function check_offset_not_negative() {
        if($this->post_data['offset'] < 0){
            http_response_code(422);
            $message = 'The offset value cant be negative.';
            echo json_encode(['message' => $message]);
            exit;
        }
    }

}