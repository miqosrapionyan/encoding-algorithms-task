<?php

trait SubstitutionEncodingValidateTrait {

    private function validate () {
        // The input values must present
        $this->check_not_empty();

        // The value lengh must be less or equal to 1000
        $this->check_value_length();

        // The character value length must be 1
        $this->check_chracters_length();
    }

    private function check_not_empty() {
        if(!$this->post_data['value'] || $this->post_data['character'] === '' || $this->post_data['replace_with'] === '') {
            http_response_code(422);
            $message = 'Please fill all required fields. *';
            echo json_encode(['message' => $message]);
            die;
        }
    }

    private function check_value_length() {
        if(strlen($this->post_data['value']) > 1000){
            http_response_code(422);
            $message = 'The encoding value length must be less or equal to 1000.';
            echo json_encode(['message' => $message]);
            exit;
        }
    }

    private function check_chracters_length() {
        if(strlen($this->post_data['character']) > 1 || strlen($this->post_data['replace_with']) > 1){
            $message = 'The characters mut be only 1.';
            echo json_encode(['message' => $message]);
            die;
        }
    }



}