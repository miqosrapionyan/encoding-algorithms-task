<?php

trait TestInputTrait{

    public function test_input(string $value): string {
        $value = trim($value);
        $value = stripslashes($value);
        $value = htmlspecialchars($value);
        return $value;
    }

}