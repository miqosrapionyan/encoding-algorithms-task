<?php

interface SanitizeInputInterface {

    public function test_input(string $value): string;

}