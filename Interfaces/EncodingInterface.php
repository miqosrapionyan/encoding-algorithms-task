<?php

interface EncodingInterface {

    public function encode() : string;

}