<?php

require_once './Interfaces/EncodingInterface.php';
require_once './Interfaces/SanitizeInputInterface.php';
require_once './Traits/TestInputTrait.php';
require_once './Traits/OffsetEncodingValidateTrait.php';

$_POST = json_decode(file_get_contents("php://input"),true);

class OffsetEncodingAlgorithm implements EncodingInterface, SanitizeInputInterface {

    use TestInputTrait, OffsetEncodingValidateTrait;

    /*
     * The post data
     */
    private $post_data;

    public function __construct($post){
        // Sanitize input values
        foreach($post as $key => $data){
            $this->post_data[$key] = $this->test_input($data);
        }
    }

    /*
     * Encodes the string
     */
    public function encode(): string {
        // If validation fails, its stops here
        $this->validate();

        $this->post_data['offset'] += 1;
        $len = strlen($this->post_data['value']);
        if($this->post_data['offset'] > $len){
            $this->post_data['offset'] -= $len;
            return $this->encode();
        }else{
            $new_str = [];
            $str_len = strlen($this->post_data['value']);
            for ($i = 0; $i < $str_len; $i++){
                $pos = $i + $this->post_data['offset'];
                if($pos > $str_len){
                    $pos -= $str_len;
                }
                $new_str[$pos] = $this->post_data['value'][$i];
            }
            ksort($new_str);
            return join("", $new_str);
        }
    }

}
if(isset($_POST['offset'])){
    $algorithm = new OffsetEncodingAlgorithm($_POST);

    $result = $algorithm->encode();
    http_response_code(200);
    echo json_encode(['result' => $result]);
}
